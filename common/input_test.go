package common

import (
	"bufio"
	"strings"
	"testing"
)

func TestReadLine(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		input    string
		expected string
	}{
		{"should read line", "test input line\n", "test input line"},
		{"should return empty", "", ""},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			bufReader := bufio.NewReader(reader)

			// reader := bufio.NewReader(buffer)
			line := ReadLine(bufReader)

			if line != tc.expected {
				t.Errorf("expected: '%s', got: '%s'", tc.expected, line)
			}
		})
	}
}
